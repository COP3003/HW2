import java.util.Set;
import java.util.EnumSet;
import java.util.Collections;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
 
public interface RomanNumerals {
  public enum Numeral {
    M(1000), CM(900), D(500), CD(400), C(100), XC(90), L(50), XL(40), X(10), IX(9), V(5), IV(4), I(1);
 
    public final int value;
 
    private static final Set<Numeral> SET = Collections.unmodifiableSet(EnumSet.allOf(Numeral.class));
 
    private Numeral(int value) {
      this.value = value;
    }
 
    public static Numeral getLargest(int value) {
      return SET.stream()
    		  
        .filter(numeral -> value >= numeral.value)
        .findFirst()
        .orElse(I)
      ;
    }
  };
 
  public static String encode(int n) {
    return IntStream.iterate(n, l -> l - Numeral.getLargest(l).value)
      .limit(Numeral.values().length)
      .filter(l -> l > 0)
      .mapToObj(Numeral::getLargest)
      .map(String::valueOf)
      .collect(Collectors.joining())
    ;
  }
 
  public static int decode(String roman) {
    int result =  new StringBuilder(roman.toUpperCase()).reverse().chars()
      .mapToObj(c -> Character.toString((char) c))
      .map(numeral -> Enum.valueOf(Numeral.class, numeral))
      .mapToInt(numeral -> numeral.value)
      .reduce(0, (a, b) -> a + (a <= b ? b : -b))
    ;
    if (roman.charAt(0) == roman.charAt(1)) {
      result += 2 * Enum.valueOf(Numeral.class, roman.substring(0, 1)).value;
    }
    return result;
  }
 
  public static void test(int n) {
    System.out.println(n + " = " + encode(n));
    System.out.println(encode(n) + " = " + decode(encode(n)));
  }
 
  public static void main(String[] args) {
    IntStream.of(1999, 25, 944).forEach(RomanNumerals::test);
  }
}