import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class arabicRomanConverter extends JFrame implements KeyListener {

	private JLabel arabicNumeral;
	private JLabel romanNumeral;
	private JTextField textField1;
	private JTextField textField2;
	private String roman;
	private int arabic;

	public arabicRomanConverter() {
		super("Roman <--> Arabic");
		setLayout(new FlowLayout());
		Container container = getContentPane();
		arabicNumeral = new JLabel("Arabic Numural                        ");
		textField1 = new JTextField(10);
		romanNumeral = new JLabel("Roman Numural                       ");
		textField2 = new JTextField(10);

		container.add(arabicNumeral);
		container.add(textField1);
		container.add(romanNumeral);
		container.add(textField2);

		textField1.addKeyListener(this);
		textField2.addKeyListener(this);

	}

	public static void main(String args[]) {
		arabicRomanConverter homework = new arabicRomanConverter();
		homework.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		homework.setSize(290, 90); // set frame size
		homework.setResizable(false);
		homework.setVisible(true); // display frame
	}

	public String arabicToRoman(int arabic) {
		if (arabic < 1 || arabic > 3999) {
			return "Invalid Roman Number Value";
		}
		roman = "";
		while (arabic >= 1000) {
			roman += "M";
			arabic -= 1000;
		}
		while (arabic >= 900) {
			roman += "CM";
			arabic -= 900;
		}
		while (arabic >= 500) {
			roman += "D";
			arabic -= 500;
		}
		while (arabic >= 400) {
			roman += "CD";
			arabic -= 400;
		}
		while (arabic >= 100) {
			roman += "C";
			arabic -= 100;
		}
		while (arabic >= 90) {
			roman += "XC";
			arabic -= 90;
		}
		while (arabic >= 50) {
			roman += "L";
			arabic -= 50;
		}
		while (arabic >= 40) {
			roman += "XL";
			arabic -= 40;
		}
		while (arabic >= 10) {
			roman += "X";
			arabic -= 10;
		}
		while (arabic >= 9) {
			roman += "IX";
			arabic -= 9;
		}
		while (arabic >= 5) {
			roman += "V";
			arabic -= 5;
		}
		while (arabic >= 4) {
			roman += "IV";
			arabic -= 4;
		}
		while (arabic >= 1) {
			roman += "I";
			arabic -= 1;
		}
		return roman;
	}

	private static int letterToNumber(char letter) {
		switch (letter) {
		case 'I':
			return 1;
		case 'V':
			return 5;
		case 'X':
			return 10;
		case 'L':
			return 50;
		case 'C':
			return 100;
		case 'D':
			return 500;
		case 'M':
			return 1000;
		default:
			return -1;
		}
	}

	private static int convertToArabic(String num) {

		int i = 0;// position of the string number
		int arabic = 0; // Arabic numeral that has been converted so far
		// while i is less than the num length
		while (i < num.length()) {

			char letter = num.charAt(i); // Letter at current position in string.
			int number = letterToNumber(letter); // Numerical equivalent of letter.

			if (number < 0)
				throw new NumberFormatException("Illegal character " + letter + " in roman numeral.");

			i++; // Move on to next position in the string

			if (i == num.length()) {// it is the last letter of the string, therefore only adds
									// the number to the single letter to arabic
				arabic += number;
			} else {
				/*
				 * Else it looks at the next letter in the string. If it has a larger Roman
				 * numeral equivalent than number, then the two letters are counted together as
				 * a Roman numeral with value (nextNumber - number).
				 */
				int nextNumber = letterToNumber(num.charAt(i));
				if (nextNumber > number) {
					// Combines the two letters to get one value
					arabic += (nextNumber - number);
					i++;// moves to the next position
				} else {
					arabic += number; // Adds the value of the one letter onto the number.
				}
			}

		} // end while

		if (arabic > 3999)
			throw new NumberFormatException("Roman numeral must have value 3999 or less.");

		return arabic;
	}

	public void keyTyped(KeyEvent e) {
	}

	public void keyPressed(KeyEvent e) {
	}

	public void keyReleased(KeyEvent e) {
		if (e.getSource().equals(textField2)) {
			if (textField2.getText().equals("")) {
				textField1.setText("");
			} else {
				textField1.setText(String.valueOf(convertToArabic(textField2.getText().toUpperCase())));
			}
		} else {
			if (textField1.getText().equals("")) {
				System.err.println("f1");
				textField2.setText("");
			} else {
				try {
					arabic = Integer.parseInt(textField1.getText());

					if (arabic > 3999) {
						textField1.setText(textField1.getText().substring(0, textField1.getText().length() - 1));
					}

					arabicToRoman(arabic);
					textField2.setText(roman);

				} catch (NumberFormatException exception) {
					textField1.setText("");
					textField2.setText("");
				}
			}
		}
	}
}
