/*
 * Copyright (c) 1995, 2008, Oracle and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - Neither the name of Oracle or the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */ 


/*
* KeyEventDemo
*/

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import javax.swing.*;

public class KeyEventDemo extends JFrame
        implements KeyListener,
        ActionListener
{
	enum RomanNum {
		  		M(1000),
		  		CM(900), D(500), CD(400), C(100), 
		  		XC(90), L(50), XL(40), X(10),
		  		IX(9), V(5), IV(4),
		  		I(1);
		int roman;
		RomanNum(int roman){
			this.roman = roman;
		}
		int toInt() {
			return roman;
		}
	};
	
    JTextArea displayArea;
    JTextField typingArea;
    String typingAreaInput = "";
    
    static final String newline = System.getProperty("line.separator");
    
/*    public static int [] parseIntArray(String pattern, String number){
    	
		Pattern p = java.util.regex.Pattern.compile(pattern);
		
		Stream<String> stream = p.splitAsStream(number);
		
		IntStream intStream = stream.mapToInt(n->RomanNum.valueOf(n).toInt());
		
		return intStream.toArray();
    } */
    
    public static int [] parseRomanArray(String pattern, String number){
		return Pattern.compile(pattern)	
					  .splitAsStream(number)	
					  .mapToInt(n->RomanNum.valueOf(n).toInt())
					  .toArray();
  }

    
    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    private static void createAndShowGUI() {
        //Create and set up the window.
        KeyEventDemo frame = new KeyEventDemo("KeyEventDemo");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        //Set up the content pane.
        frame.addComponentsToPane();
        
        
        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }
    
    private void addComponentsToPane() {
        
        JButton button = new JButton("Clear");
        button.addActionListener(this);
        
        typingArea = new JTextField(20);
        typingArea.addKeyListener(this);
        
        //Uncomment this if you wish to turn off focus
        //traversal.  The focus subsystem consumes
        //focus traversal keys, such as Tab and Shift Tab.
        //If you uncomment the following line of code, this
        //disables focus traversal and the Tab events will
        //become available to the key event listener.
        //typingArea.setFocusTraversalKeysEnabled(false);
        
        displayArea = new JTextArea();
        displayArea.setEditable(false);
        JScrollPane scrollPane = new JScrollPane(displayArea);
        scrollPane.setPreferredSize(new Dimension(375, 125));
        
        getContentPane().add(typingArea, BorderLayout.PAGE_START);
        getContentPane().add(scrollPane, BorderLayout.CENTER);
        getContentPane().add(button, BorderLayout.PAGE_END);
    }
    
    public KeyEventDemo(String name) {
        super(name);
    }
    
    
    /** Handle the key typed event from the text field. */
    public void keyTyped(KeyEvent e) {
//        displayInfo(e, "KEY TYPED: ");
//    	System.err.println(String.valueOf(e.getKeyChar()).toUpperCase());
//    	System.err.println(typingArea.getText());
    }
    
    /** Handle the key pressed event from the text field. */
    public void keyPressed(KeyEvent e) {
        displayInfo(e, "KEY PRESSED: ");
    }
    
    /** Handle the key released event from the text field. */
    public void keyReleased(KeyEvent e) {
        displayInfo(e, "KEY RELEASED: ");
    }
    
    /** Handle the button click. */
	public void actionPerformed(ActionEvent e) {
		// Clear the text components.
		displayArea.setText("");
		typingArea.setText("");
		typingAreaInput = "";
		// Return the focus to the typing area.
		typingArea.requestFocusInWindow();
	}

	private void displayArabic(String str) {
		/*
		 * regex should be removing the matches found and returning the unmatched string.
		 * Prof Guo:
		 * 	use replaceall to check for an empty string. If so then the input is valid.
		 * 	use matcher to get the valid string.
		 * 	but if the string is valid, then I can just use the string.
		 * 		alternate is have 4 patterns, each allows a single group through
		 */
		List<String> pattern = new ArrayList<>(Arrays.asList(
							"(M{0,3})(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})",
							"(?<hun>CM|CD|D?C{0,3})(?<ten>XC|XL|L?X{0,3})(?<one>IX|IV|V?I{0,3})",
							"(M{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})",
							"(M{0,3})(CM|CD|D?C{0,3})(IX|IV|V?I{0,3})",
							"(M{0,3})(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})"));
		

//		int[] out;
		List<Pattern> pl = new ArrayList<>();
		for (String s : pattern) {
			pl.add(Pattern.compile(s));
		}
//		pl = pattern.s;//.((p -> Pattern.compile(p));
//		Pattern p = Pattern.compile(pattern.get(0));
		Matcher m = pl.get(0).matcher("MMDCCXCVII");//"MMDCCXCVII"
		m.find();
//		System.err.println(m.group(1)+","+m.group(4));// will capture valid
		System.err.println(m.replaceAll(""));// tests if there is an invalid character
		if (m.replaceAll("").equals("")) {
//			for (Pattern p : pl) {
				System.err.println(m.group(1)+","+m.group(4));
//			}
//			System.err.println();
		}
//		if (Pattern.matches(pattern, "MMXI")) {
////			out = parseIntArray(pattern, str);
//			System.err.println("out: " + );
//		}
		
//		RomanNum.valueOf("X").toInt();
//		List<RomanNum> romans = new ArrayList<>();
//		str.chars().mapToObj(Character::valueOf).map(RomanNum::valueOf).forEach(romans::add);
//		int value = 0;
//		while (!romans.isEmpty()) {
//		    RomanNum current = romans.remove(0);
//		    if (!romans.isEmpty() && current.shouldCombine(romans.get(0))
//		        value += current.toInt(romans.remove(0));
//		    else
//		        value += current.ToInt();
//		}
//		Pattern p = Pattern.compile(pattern);
//		 public String[] split(CharSequence input, int limit)
//		RomanNum value = RomanNum.valueOf(m.group());
//		System.err.println("vLUE: " + value);
//		String[] romanArray = p.split(str, -2);
//		String out = "";
//		for (String s : romanArray) out += s;
//		System.err.println("p: " + p.toString() + "\nrA: " + romanArray.toString() + "\nrA[0]: " + romanArray[0]);
		
		//((Pattern.matches(pattern, str))?true:false)
//		displayArea.append(out + newline);
		displayArea.setCaretPosition(displayArea.getDocument().getLength());

	}
    /*
     * We have to jump through some hoops to avoid
     * trying to print non-printing characters
     * such as Shift.  (Not only do they not print,
     * but if you put them in a String, the characters
     * afterward won't show up in the text area.)
     */
    private void displayInfo(KeyEvent e, String keyStatus){
        
        //You should only rely on the key char if the event
        //is a key typed event.
        int id = e.getID();
        String keyString;
        if (id == KeyEvent.KEY_TYPED) {
            char c = e.getKeyChar();
            keyString = "key character = '" + c + "'";
        } else {
            int keyCode = e.getKeyCode();
            keyString = "key code = " + keyCode
                    + " ("
                    + KeyEvent.getKeyText(keyCode)
                    + ")";
        }
        
        int modifiersEx = e.getModifiersEx();
        String modString = "extended modifiers = " + modifiersEx;
        String tmpString = KeyEvent.getModifiersExText(modifiersEx);
        if (tmpString.length() > 0) {
            modString += " (" + tmpString + ")";
        } else {
            modString += " (no extended modifiers)";
        }
        
        String actionString = "action key? ";
        if (e.isActionKey()) {
            actionString += "YES";
        } else {
            actionString += "NO";
        }
        
        String locationString = "key location: ";
        int location = e.getKeyLocation();
        if (location == KeyEvent.KEY_LOCATION_STANDARD) {
            locationString += "standard";
        } else if (location == KeyEvent.KEY_LOCATION_LEFT) {
            locationString += "left";
        } else if (location == KeyEvent.KEY_LOCATION_RIGHT) {
            locationString += "right";
        } else if (location == KeyEvent.KEY_LOCATION_NUMPAD) {
            locationString += "numpad";
        } else { // (location == KeyEvent.KEY_LOCATION_UNKNOWN)
            locationString += "unknown";
        }
        
        displayArea.append(keyStatus + newline
                + "    " + keyString + newline
                + "    " + modString + newline
                + "    " + actionString + newline
                + "    " + locationString + newline);
        displayArea.setCaretPosition(displayArea.getDocument().getLength());
    }
    
    
    public static void main(String[] args) {
//        /* Use an appropriate Look and Feel */
//        try {
//            //UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
//            //UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
//            UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
//        } catch (UnsupportedLookAndFeelException ex) {
//            ex.printStackTrace();
//        } catch (IllegalAccessException ex) {
//            ex.printStackTrace();
//        } catch (InstantiationException ex) {
//            ex.printStackTrace();
//        } catch (ClassNotFoundException ex) {
//            ex.printStackTrace();
//        }
//        /* Turn off metal's use of bold fonts */
//        UIManager.put("swing.boldMetal", Boolean.FALSE);
//        
//        //Schedule a job for event dispatch thread:
//        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }
    
}