import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.stream.Collectors.toList;

public class RegexTestStrings {
	int a=1,t=0;
    String[][] in = {{"Roman","MMMDCCCLXXXVIII","MCMXCIX","MMMI"}, //[0]=Roman
    				 {"Arabic","3999","1234","0045","1","2040","4"}};  //[1]=Arabic
    List<String> pA = Arrays.asList(
    		"(M{0,3})(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})",//0
    		    "(CM|CD)?(XC|XL)?(IX|IV)?(M?)(M?)(M?)(D?)(C?)(C?)(C?)"
						  	      + "(L?)(X?)(X?)(X?)(V?)(I?)(I?)(I?)",//1
				  	      			"(L?)(X?)(X?)(X?)(V?)(I?)(I?)(I?)",//2
		  	      					"(L)?(X)?(X)?(X)?(V?)(I?)(I?)(I?)",//3
		  	      						  "^(0*)([1-3]?)([0-9]{0,3})$",//4
		  	      						  					 "^0*(.*)",//5
      						  	  "^0*([1-3]?)([0-9]?)([0-9]?)([0-9]?)"//6
    );
    ArrayList<Pattern> pC = (ArrayList<Pattern>) pA.stream()
    		.map(Pattern::compile)
    		.collect(toList());
    /* constructor */
    RegexTestStrings(){
//    	System.err.println(Numeral.getRoman(500));
    	StringBuilder sb = new StringBuilder();
    	StringBuilder sx = new StringBuilder();
    	sb.append("Type["+in[t][0]+"]\n"); // Roman or Arabic
    	for (int i=1;i<in[t].length;i++) { // loop through inputs
        	sb.append("\tInput["+in[t][i]+"]\n");
        	int j = 0; sx.setLength(0);
        	if(t==1 && Integer.parseInt(in[t][i]) > 0) {
        		List<String> gList = mg(pC.get(a), in[t][i]);
        		sb.append("\t\tnumeral: "+toString(gList)+"\n");
        		for (String s : gList) {
        			sb.append("\t\tgroup["+j+"]: "+s+" <--> "+"\n");
//        					((!(s.isEmpty()||s == "0"||s == null))?"\n"
//        							:Numeral.getRoman(
//        							Integer.parseInt(s))+"\n"));
        			j++;sx.append(s);
        		}
        	} else if (t == 0) {
        		List<String> gList = mg(pC.get(a), in[t][i]);
        		sb.append("\t\tnumeral: "+toString(gList)+"\n");
        		for (String s : gList) {
        			sb.append("\t\tgroup["+j+"]: "+s+" <--> "+"\n");
        			j++;sx.append(s);
        		}
        	}else sb.append("\t\tInvalid\n");
    		sb.append("\t\t"+sx.toString()+"\n");
    	}
    	System.err.println(sb.toString());
    	System.err.println(toString(mg(pC.get(a),in[t][1])));
    }

    List<String> mg(Pattern p, String s){
    	Matcher m = p.matcher(s);
    	List<String> gL = new ArrayList<>();
    	m.find();
		for (int i = 1; i <= m.groupCount(); i++) { //group 0 should be empty
    		gL.add(m.group(i));
		}
    	return gL;
    }

    public String toString(List<String> groupList) {
    	StringBuilder numeral = new StringBuilder();
    	for (String s : groupList) {
    		numeral.append(s);
    	}
    	return numeral.toString();
    }
    
	enum Numeral {
		M(1000), 
		CM(900), D(500), CD(400), C(100),
		XC(90), L(50), XL(40), X(10),
		IX(9), V(5), IV(4), I(1);
		static List<Integer> ARABIC = new ArrayList<Integer>(Arrays.asList(
				1000,900,500,400,100,90,50,40,10,9,5,4,1));

		int arabic;
		String roman;
		
		private static class Maps {
			static Map<Integer, Numeral> MAP = new HashMap<>();
		}

		private Numeral(int arabic) {
			this.arabic = arabic;
			roman = this.toString();
			Maps.MAP.put(arabic, this);
		}

		public int getArabic() {
			return arabic;
		}
		
		public static String getRoman(int roman) {
			Numeral n = Maps.MAP.get(roman);
			return n.toString();
		}
	};
    
    public static void main(String[] args) {
    	new RegexTestStrings();
    }
}
//System.out.println(TEST[t].matches(pA[a]));
//String[] splitString = (TEST[t].split(pA[a]));
//System.out.println(splitString.length);// should be 14
//for (String string : splitString) {
//  System.out.println(string);
//}
//// replace all whitespace with tabs
//System.out.println(TEST[t].replaceAll(pA[a],""));