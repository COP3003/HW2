import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A simple numeral converter bilaterally between Arabic and Roman.
 * 
 * @author 5153
 */
public class ArabicToRoman extends JFrame implements KeyListener {
	private final JTextField arabicArea;     // Textfield for Arabic numeral entry
	private final JTextField romanArea;      // Textfield for Roman numeral entry
	private final ConvertNumerals convert;   // class that handles the conversion process

  /**
   * Constructor sets up the JFrame and components.
   */
  private ArabicToRoman() {
		super("Roman <--> Arabic");

		// JFrame setup
		GridLayout gridLayout = new GridLayout(2, 2, 0, 6);
		Container container = getContentPane();
		setLayout(gridLayout);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(300, 100);
		setResizable(false);
		
		// Initialize globals
		convert = new ConvertNumerals();

		// Arabic text box label
		JLabel arabicLabel = new JLabel("Arabic Numeral");
		arabicLabel.setFocusTraversalKeysEnabled(false);
		arabicLabel.setBorder(null);
		add(arabicLabel);

		// Arabic text box
		arabicArea = new JTextField(4);
		arabicArea.addKeyListener(this);
		arabicArea.setFocusTraversalKeysEnabled(false);
		arabicArea.setText("");
		add(arabicArea);

		// Roman text box label
		JLabel romanLabel = new JLabel("Roman Numeral");
		romanLabel.setFocusTraversalKeysEnabled(false);
		romanLabel.setBorder(null);
		add(romanLabel);

		// Roman text box
		romanArea = new JTextField(0);
		romanArea.addKeyListener(this);
		romanArea.setFocusTraversalKeysEnabled(false);
		romanArea.setText("");
		add(romanArea);

		container.setVisible(true);
	}

  /**
   * static container of all the Roman characters and their Arabic values
   */
	enum Numeral {
		M(1000), 
		CM(900), D(500), CD(400), C(100),
		XC(90), L(50), XL(40), X(10),
		IX(9), V(5), IV(4), I(1);

		// Descending ordered list of Arabic values.
    // Matches the order of Roman numerals.
		static final List<Integer> ARABIC = new ArrayList<>(Arrays.asList(
								1000,900,500,400,100,90,50,40,10,9,5,4,1));
		
		final int arabic;
		final String roman;

		// Arabic to Roman value to character map.
		private static class Maps {
			static final Map<Integer, Numeral> MAP = new HashMap<>();
		}

    /**
     * enum constructor
     * @param arabic arabic value
     */
		Numeral(int arabic) {
			this.arabic = arabic;
			roman = this.toString();
			Maps.MAP.put(arabic, this);
		}

		// returns the Arabic value of a Roman numeral.
    int getArabic() {
			return arabic;
		}

		// returns the Roman numeral from an Arabic value
		static String getRoman(int roman) {
			Numeral n = Maps.MAP.get(roman);
			return n.toString();
		}
	}
	
	/**
	 * Internal class that bilaterally converts Roman and Arabic numerals.
	 * Valid only for values ranged from 1-3999.
	 *
	 */
	private class ConvertNumerals {
		private String lastValidRoman;	// last valid roman output
		private String lastValidArabic; // last valid arabic output
		final Pattern romanPattern1;		// regex pattern to validate roman numbers
		final Pattern romanPattern2;		// regex pattern to convert roman numbers
		final Pattern arabicPattern;	// regex pattern for valid arabic numbers

    /**
     * Constructor initializes class variables.
     */
    ConvertNumerals(){
			lastValidRoman = "";
			lastValidArabic = "";
			romanPattern1 = Pattern.compile("(M{0,3})(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})"
                                      + "(IX|IV|V?I{0,3})");
			romanPattern2 = Pattern.compile("(CM|CD)?(XC|XL)?(IX|IV)?(M?)(M?)"+
					   "(M?)(D?)(C?)(C?)(C?)(L?)(X?)(X?)(X?)(V?)(I?)(I?)(I?)");
			arabicPattern = Pattern.compile("^0*([1-3]?)([0-9]?)([0-9]?)([0-9]?)");
		}

    /**
     * Sets both input text fields to empty strings.
     */
    void empty() {
			lastValidRoman = "";
			lastValidArabic = "";
		}

    /**
     * Tests an input string against a regex pattern.
     * @param p Pattern regex pattern to match against
     * @param s String string to test the pattern
     * @return boolean true if the input string contains only valid characters
     */
    boolean isValid(Pattern p, String s){
			return p.matcher(s).replaceAll("").equals("");
		}

    /**
     * Validates Roman numeral input. Wrapper method for isValid.
     * @param s String user input from Roman text box
     * @return boolean True if input contains only valid characters
     */
    boolean isValidRoman(String s) {
			return isValid(romanPattern1, s);
		}

    /**
     * Validates Arabic numeral input. Wrapper method for isValid.
     * @param s String user input from Arabic text box
     * @return boolean True if input contains only valid characters
     */
    boolean isValidArabic(String s) {
			return isValid(arabicPattern, s);
		}

    /**
     * returns the captured elements from a regex pattern
     * @param p Pattern regex pattern to match against
     * @param s String user input string
     * @return ArrayList of strings of individual numerals from the input string
     */
    private List<String> split(Pattern p, String s){
      Matcher m = p.matcher(s);

      // list of captured pattern matches
      List<String> groupList = new ArrayList<>();
      m.find(); // capture the matched characters

      // loop through the match groups and add the strings to the list
      for (int i = 1; i <= m.groupCount(); i++) { //group 0 should be empty
        if (!(m.group(i) == null || m.group(i).isEmpty())) {
          groupList.add(m.group(i));
        }
      }
      return groupList;
    }

    /**
     * First pattern gets only valid structured roman numerals.
     * Second pattern will be applied to each group to grab
     * individual numerals and add them to a single list.
     * Wrapper for the split method.
     * @param romanText String roman numeral input string
     * @return ArrayList of individual numerals from the input string
    */
    List<String> splitRoman(String romanText){
      // list of captured pattern matches
    	List<String> groupList = new ArrayList<>();

    	// Loop through list returned from first pattern matches
    	for (String g : split(romanPattern1, romanText)){
    	  // separate individual numerals using second pattern
        // gives a single flat list of numerals
        groupList.addAll(split(romanPattern2, g));
			}
      return groupList;
    }

    /**
     * Wrapper method for split specific to Arabic input
     * @param s input string from Arabic text box
     * @return ArrayList of Arabic numerals in descending order
     */
    List<String> splitArabic(String s){
			return split(arabicPattern, s);
		}

    /**
     * Concatenates a List of numerals into a single string
     * @param groupList List of numerals
     * @return String numerals in a single ordered string
     */
    String toString(List<String> groupList) {
      StringBuilder numeral = new StringBuilder();
      for (String s : groupList) {
        numeral.append(s);
      }
      return numeral.toString();
    }

    /**
     * Calculates the conversion values and sets class variables used to update
     * the text boxes.
     * @param numeralList valid list of individual numerals
     */
    void value(List<String> numeralList) {
      String numeralString = toString(numeralList);

      // If the List reduces to an Arabic number otherwise its a Roman numeral
      if(numeralString.matches("\\d+")) {

        // convert from Arabic to Roman numerals and update text boxes
        lastValidRoman = toRoman(Integer.parseInt(numeralString));
        lastValidArabic = numeralString;
      } else {
        // convert from Roman to Arabic numerals and update text boxes
        lastValidArabic = toArabic(numeralList);
        lastValidRoman = numeralString;
      }
    }

    /**
     * Calculate the conversion of Arabic to Roman
     * @param numberal Arabic integer
     * @return String converted Roman numeral
     */
    String toRoman(int numberal) {
      String numeralString = "";
      for (int a : Numeral.ARABIC) {
        if (numberal == 0) {
          break;
        } else {
          while (numberal / a != 0) {
            numeralString += Numeral.getRoman(a);
            numberal -= a;
          }
        }
      }
      return numeralString;
    }

    /**
     * Calculate the conversion of Roman to Arabic
     * @param numeralList List of separate Roman characters
     * @return String converted Arabic numeral
     */
    String toArabic(List<String> numeralList) {
      int numeral = 0;
      for(String r : numeralList) {
        if (r != null && !r.isEmpty()) {
          numeral += Numeral.valueOf(r).getArabic();
        }
      }
      return String.valueOf(numeral);
    }
	}
	
  @Override
  public void keyPressed(KeyEvent e) {
  }
  
  @Override
  public void keyTyped(KeyEvent e) {
  }

  /**
   * KeyEvent that triggers on the release of a key. Used to fire the conversion
   * process and update text input boxes.
   * @param e Event object with all the Event data
   */
  @Override
  public void keyReleased(KeyEvent e) {
    // cursor location when event fires
	  int caret = ((JTextField) e.getSource()).getCaretPosition();

	  // determine which text box the key press was in. First is Arabic.
	  if (e.getSource().equals(arabicArea)) {

	    // Test if the text box was cleared. If so clear the other text box.
		  if (arabicArea.getText().isEmpty() || arabicArea.getText() == null) {
			  convert.empty();

			// Test if the input is valid, otherwise replace the text box with the
      //  last good text
		  } else if (!convert.isValidArabic(arabicArea.getText().toUpperCase())){
			  arabicArea.setText(convert.lastValidArabic);

			// convert from Arabic to Roman and update both text boxes
		  } else {
			  convert.value(convert.splitArabic(arabicArea.getText().toUpperCase()));
		  }

		// Roman text box processing.
	  }	else if (e.getSource().equals(romanArea)){

      // Test if the text box was cleared. If so clear the other text box.
		  if (romanArea.getText().isEmpty() || romanArea.getText() == null) {
			  convert.empty();

      // Test if the input is valid, otherwise replace the text box with the
      //  last good text
		  } else if (!convert.isValidRoman(romanArea.getText().toUpperCase())){
			  romanArea.setText(convert.lastValidRoman);

      // convert from Roman to Arabic and update both text boxes
		  } else {
			  convert.value(convert.splitRoman(romanArea.getText().toUpperCase()));
		  }
	  }
	  arabicArea.setText(convert.lastValidArabic);
	  romanArea.setText(convert.lastValidRoman);

	  // Place the cursor at the last position before updating the text boxes.
	  ((JTextField) e.getSource()).setCaretPosition((caret >
	  				   ((JTextField) e.getSource()).getText().length())?
					   ((JTextField) e.getSource()).getText().length():caret);
  }

  // Main creates the converter object and makes it visible.
  public static void main(String[] args){
	ArabicToRoman  a2r = new ArabicToRoman();
	a2r.setVisible(true); 
  }
}